KVM Forum 2024 will be held at [Hotel
International](https://maps.app.goo.gl/qVJU3aW8gDq385Lj9) in Brno,
Czech Republic.

The venue is in the city center and is within walking distance from
the train station or Náměsti Svobody.

## Getting around in Brno

The easiest way to use trams, trolleybuses or buses in Brno is
to tap a credit card against the yellow reader every time you
board a vehicle and when you leave. The system will charge you
90 CZK at maximum per day.

Alternatively, you can buy a 5-day ticket for 250 CZK at the main railway
station.  You can buy it at any counter (look for the international
counters because they are more likely to speak English). When you first
use your ticket, be sure to validate it. You only have to validate your
ticket one time, not on each trip.

## Getting to Brno

Brno has a small international airport with flights from London (Stansted)
and other European cities.  If you arrive there, bus 40 will take you
directly to the hotel.

Other nearby airports include Vienna, Bratislava and Prague. Travelling
to Brno is easiest from Vienna Schwechat Airport, from where there are
direct buses operated by [RegioJet](https://regiojet.com/?fromLocationId=10204055&toLocationId=10202002)
(formerly known as Student Agency).

Attendees of the Linux Plumbers Conference can travel from Vienna to Brno
by train on Saturday, September 21.

If you need a visa invitation letter, please reach out to the organizers at
[kvm-forum-2024-pc@redhat.com](mailto:kvm-forum-2024-pc@redhat.com).

## Accomodation

Special room prices are available at Hotel International for attendees
of KVM Forum. More information will be available soon.
