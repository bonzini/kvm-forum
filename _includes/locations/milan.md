KVM Forum 2024 will be held at [Hotel Novotel Milano Nord Ca'
Granda](https://maps.app.goo.gl/TaR6kei9LR4Q7pnM6) in Milan, Italy.

## Getting around Milan

Milan's public transport system consists of five subway lines and many
tramway and bus lines.

The venue is a 5 minutes walk from the <i>Ca' Granda</i> stop of the
"purple" M5 subway line.  To reach downtown Milan, take the subway
until the Isola or Garibaldi FS stops.

The easiest way to use public transport in
Milan is to tap a credit card against the readers every time you board a
vehicle or pass the subway barriers, as well as when you leave the subway.
In particular, when leaving the subway, note that not all barriers have
a credit card reader and the barriers may be open during peak hours,
but you still have to tap.  The system combines multiple trips taken
during a single day in order to calculate the cheapest fare.  For more
information, see the [ATM contactless paying
web page](https://www.atm.it/en/ViaggiaConNoi/Pages/contactless_cards_metro.aspx).

## Getting to Milan

The main airport in Milan is Milano Malpensa (MXP).  It is well connected
by trains to the city center and to the M5 subway lines.

Two more airports are available, but reaching Ca' Granda is a bit less
comfortable.  Linate (LIN) is a city airport that is connected to
downtown by the "blue" M4 subway line; Bergamo (BGY) hosts low-cost
companies and it is connected to the city center by buses.

Flights are available between the Milan area and most European countries,
as well as from America and Asia to Malpensa.

If you need a visa invitation letter, please reach out to the organizers at
[kvm-forum-2024-pc@redhat.com](mailto:kvm-forum-2024-pc@redhat.com).

## Accomodation

Special room prices will be available at Novotel and the nearby ibis Milano
Nord Ca' Granda for KVM Forum attendees.  More information will be provided
soon via Eventbrite.
