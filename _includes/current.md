{% assign edition = site.editions[-1] %}

## Announcing KVM Forum {{ edition.title }}!

KVM Forum is an annual event that presents a rare opportunity for
developers and users to discuss the state of Linux virtualization
technology and plan for the challenges ahead. Sessions include updates
on the state of the KVM virtualization stack, planning for the future,
and many opportunities for attendees to collaborate.  Birds of a feather
(BoF) sessions facilitate discussion of strategic decisions.

The next KVM Forum will be held in **[{{ edition.place }}](/location)** on
**{{ edition.dates | replace: "-", "&ndash;" }}**.

## Deadlines

Proposals for speaking at KVM Forum should be submitted on [Pretalx]({{edition.submit}}).
The **deadline** for submitting presentations is **June 7, 2024 - 11:59 PM PDT**.

Accepted speakers will be notified on **July 5, 2024**.

## Attending KVM Forum

{% if edition.register %}
The cost of admission to KVM Forum {{ edition.title }} is $75. You can
register [here]({{edition.register}}).  Admission is free for accepted speakers.
{% else %}
Registration to KVM Forum {{ edition.title }} is not open yet.
{% endif %}

We are committed to fostering an open and welcoming environment at our
conference. Participants are expected to abide by our [code
of conduct](coc/) and [media policy](media-policy/).
