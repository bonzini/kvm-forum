---
title: Archive
heading: "All editions"
layout: home
permalink: /archive/
---
{% include relative_root.html %}

Links to videos and slides are available for the following editions of KVM Forum:

<dl>
{% for edition in site.editions reversed %}
{% if edition.content == "" %}{% continue %}{% endif %}
<dt><a href="{{ relative_root }}/{{ edition.title }}/">{{ edition.title }}</a></dt>
<dd>{{ edition.place | default: "Virtual conference"}} &mdash; {{ edition.dates | replace: "-", "&ndash;" }}</dd>
{% endfor %}
</dl>
