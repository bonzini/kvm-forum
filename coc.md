---
title: Code of conduct
heading: "Code of conduct"
layout: home
permalink: /coc/
---
{% include relative_root.html %}

## Our Pledge

In the interest of fostering an open and welcoming environment, we
as speakers, attendees and organizers at {{ site.title }} pledge to making
participation in our event and community a harassment-free experience
for everyone, regardless of age, body size, disability, ethnicity,
gender identity and expression, level of experience, nationality,
personal appearance, race, religion, or sexual identity and orientation.

## Our Standards

Examples of behavior that contributes to creating a positive environment include:

* Using welcoming and inclusive language
* Being respectful of differing viewpoints and experiences
* Gracefully accepting constructive criticism
* Focusing on what is best for the community
* Showing empathy towards other community members

Examples of unacceptable behavior by participants include:

* The use of sexualized language or imagery and unwelcome sexual attention or advances
* Trolling, insulting/derogatory comments, and personal or political attacks
* Public or private harassment
* Publishing others private information, such as a physical or electronic address, without explicit permission
* Other conduct which could reasonably be considered inappropriate in a professional setting

## Our Responsibilities

Event organizers have the right and responsibility to remove, edit, or
reject content, posts, examples and other event contributions that are not
aligned to this Code of Conduct, or to ban temporarily or permanently any
contributor for other behaviors that they deem inappropriate, threatening,
offensive, or harmful.

## Scope

This Code of Conduct applies in physical and virtual spaces and in any
interactions relating to an individual participating in or representing
the event. In particular, this Code of Conduct applies to participating
in speaker coaching and in attendee coaching, if it's offered by the
event. Examples of representing an event include using an official event
e-mail address and posting to, sharing or referencing official social
media channels.

## Enforcement

Instances of abusive, harassing, or otherwise unacceptable behavior may
be reported to the {{ site.title }} program committee by emailing
[kvm-forum-{{edition.title}}-pc@redhat.com](mailto:kvm-forum-{{edition.title}}-pc@redhat.com).
Program committee members will also be on site and it will be possible
to contact them individually.

All complaints will be reviewed and investigated and will result
in a response that is deemed necessary and appropriate to the
circumstances. Event organizers are obligated to maintain confidentiality
with regard to the reporter of an incident and will consult with the
reporter if further action on the report is likely to make it known to
anyone who they are.

Event organizers who do not follow or enforce the Code of Conduct in
good faith may face temporary or permanent repercussions as determined
by other event organizers

_This Code of Conduct is adapted from the [Contributor Covenant, version
1.4](http://contributor-covenant.org/version/1/4)._
