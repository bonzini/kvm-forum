---
title: Sponsoring KVM Forum
layout: page
permalink: /sponsorship/
---
{% include relative_root.html %}

Sponsoring KVM Forum helps bringing together a diverse community of
developers for Linux-based virtualization and emulation techologies,
including KVM, QEMU, VFIO, virtio and more.

Sponsorships help making the conference successful and affordable,
and demonstrate support for the work of the open source virtualization
community.

Three levels of sponsorships are available:

<div class="sponsorship">
{% markdown sponsorship-levels.md %}
</div>

If you are interested in sponsoring KVM Forum, please check out the
[sponsorship agreement]({{ relative_root }}/sponsorship-contract-2024.pdf) and contact
the organizers at [kvm-forum-2024-pc@redhat.com](mailto:kvm-forum-2024-pc@redhat.com).

{% include sponsors.html %}
