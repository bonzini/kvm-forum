---
name: Oliver Upton
role: "Staff Software Engineer - Google"
---
Oliver currently works in Google Cloud, focused on virtualizing the
ARM architecture and co-maintaining KVM/arm64. Prior to KVM he
worked on embedded systems and ISP device management protocols.
