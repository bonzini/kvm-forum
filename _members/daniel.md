---
name: Daniel Berrangé
role: Senior Principal Software Engineer - Red Hat
---
Daniel is a Senior Principal Software Engineer, working in a variety
of roles at Red Hat over the last 22 years. Since 2006, he has been
specialized in the development of technologies related to virtualization
management, as lead developer of Libvirt, GTK-VNC, Libvirt Perl, Libvirt
GObject and Libvirt Sandbox, and contributor to the Xen, KVM, oVirt and
OpenStack projects. Daniel is a passionate believer in the value of open
source software and the benefits it brings to the world. Daniel is also
creator and maintainer of the Bichon GitLab terminal code review
application, and the Entangle remote camera control & capture software.
