---
name: Alexander Graf
role: Upstream Maintainer - SUSE
---
Alexander started working for SUSE in 2007. Since then he worked on fancy
things like SUSE Studio, QEMU, KVM and openSUSE on ARM. Whenever something
really useful comes to his mind, he tends to implement it. Among others
he did Mac OS X virtualization using KVM, nested SVM, KVM on PowerPC and
a lot of work in QEMU for openSUSE on ARM. He is the upstream maintainer
of KVM for PowerPC, QEMU for PowerPC and QEMU for S390x.
