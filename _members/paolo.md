---
name: Paolo Bonzini
role: "Distinguished Engineer - Red Hat"
---
Paolo Bonzini works on virtualization for Red Hat, where he is a
Distinguished Engineer. He is currently the maintainer of the KVM
hypervisor and a contributor and submaintainer for QEMU.
