---
name: Jan Kiszka
role: Senior Key Expert - Siemens AG
---
Jan Kiszka is working as consultant and senior software engineer in the
Competence Center for Embedded Linux at Siemens Corporate Technology. He
is supporting Siemens sectors with adapting and enhancing open source
as platform for their products. For customer projects and whenever
his spare time permits, he is contributing to open source projects,
specifically in the area of real-time and virtualization.
