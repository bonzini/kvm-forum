---
dates: "August 19-21, 2015"
place: "Seattle, WA"
layout: archive
talks: [
  {
    "speakers": "Paolo Bonzini",
    "time": "2015-08-19T09:00:00-07:00",
    "title": "Keynote Session: KVM Status Report",
    "slides": []
  },
  {
    "speakers": "Rik Van Riel",
    "time": "2015-08-19T09:15:00-07:00",
    "title": "Real-Time KVM",
    "slides": []
  },
  {
    "speakers": "Jan Kiszka",
    "time": "2015-08-19T10:00:00-07:00",
    "title": "Real-Time KVM for the Masses",
    "slides": []
  },
  {
    "speakers": "Mark Kraeling",
    "time": "2015-08-19T11:15:00-07:00",
    "title": "Virtualizing the Locomotive: Ready, Set, Go!",
    "slides": []
  },
  {
    "speakers": "Jens Freimann",
    "time": "2015-08-19T12:00:00-07:00",
    "title": "Pushing the limits: 1000 guests per host and beyond",
    "slides": []
  },
  {
    "speakers": "Alexander Graf",
    "time": "2015-08-19T14:00:00-07:00",
    "title": "Migratable 40GBit/s Ethernet",
    "slides": []
  },
  {
    "speakers": "Eddie Dong, Weidong Han, Hongyang Yang",
    "time": "2015-08-19T14:30:00-07:00",
    "title": "Status update on KVM-COLO HA/FT solution",
    "slides": []
  },
  {
    "speakers": "Stefan Hajnoczi",
    "time": "2015-08-19T15:00:00-07:00",
    "title": "virtio-vsock: Zero-configuration host/guest communication",
    "slides": []
  },
  {
    "speakers": "Christopher Covington",
    "time": "2015-08-19T16:00:00-07:00",
    "title": "Using Upstream QEMU for Computer Architecture and Software Studies",
    "slides": []
  },
  {
    "speakers": "David Hildenbrand",
    "time": "2015-08-19T16:30:00-07:00",
    "title": "Guest operating system debugging",
    "slides": []
  },
  {
    "speakers": "Michael S. Tsirkin",
    "time": "2015-08-19T17:00:00-07:00",
    "title": "virtio 1 - why do it? and are we there yet?",
    "slides": []
  },
  {
    "speakers": "Alexander Graf",
    "time": "2015-08-20T09:00:00-07:00",
    "title": "Keynote Session: QEMU status report",
    "slides": []
  },
  {
    "speakers": "Alex Bennée, Frederic Konrad",
    "time": "2015-08-20T09:15:00-07:00",
    "title": "Towards multi-threaded TCG",
    "slides": []
  },
  {
    "speakers": "David Matlack",
    "time": "2015-08-20T10:00:00-07:00",
    "title": "KVM Message Passing Performance",
    "slides": []
  },
  {
    "speakers": "Marc Zyngier",
    "time": "2015-08-20T11:15:00-07:00",
    "title": "ARM: Caches that give you enough rope to shoot yourself in the foot",
    "slides": []
  },
  {
    "speakers": "Gerd Hoffmann",
    "time": "2015-08-20T11:15:00-07:00",
    "title": "QEMU and OpenGL.",
    "slides": []
  },
  {
    "speakers": "Edgar E. Iglesias",
    "time": "2015-08-20T12:00:00-07:00",
    "title": "QEMU for Xilinx ZynqMP",
    "slides": []
  },
  {
    "speakers": "Markus Armbruster",
    "time": "2015-08-20T12:00:00-07:00",
    "title": "QEMU interface introspection: from hacks to solutions",
    "slides": []
  },
  {
    "speakers": "Andreas Färber",
    "time": "2015-08-20T14:00:00-07:00",
    "title": "CPU hot-plug - status and challenges",
    "slides": []
  },
  {
    "speakers": "Vladimir Sementsov-Ogievskiy, John Snow",
    "time": "2015-08-20T14:00:00-07:00",
    "title": "Incremental backups: Good things come in small packages!",
    "slides": []
  },
  {
    "speakers": "Cleber Rosa",
    "time": "2015-08-20T14:00:00-07:00",
    "title": "Avocado: Next generation virt testing",
    "slides": []
  },
  {
    "speakers": "Max Reitz, Kevin Wolf",
    "time": "2015-08-20T14:30:00-07:00",
    "title": "qcow2: why (not)?",
    "slides": []
  },
  {
    "speakers": "Michael Roth",
    "time": "2015-08-20T14:30:00-07:00",
    "title": "QEMU Hotplug Infrastructure and Implementing PCI Hotplug for PowerKVM",
    "slides": []
  },
  {
    "speakers": "Cleber Rosa",
    "time": "2015-08-20T14:30:00-07:00",
    "title": "Avocado: Next generation virt testing",
    "slides": []
  },
  {
    "speakers": "Richard W.M. Jones",
    "time": "2015-08-20T15:30:00-07:00",
    "title": "New qemu technology used in virt-v2v",
    "slides": []
  },
  {
    "speakers": "Bandan Das, Eyal Moscovici",
    "time": "2015-08-20T15:30:00-07:00",
    "title": "Vhost: Sharing is better",
    "slides": []
  },
  {
    "speakers": "Julia Lawall",
    "time": "2015-08-20T15:30:00-07:00",
    "title": "Getting Started with Coccinelle (KVM edition)",
    "slides": []
  },
  {
    "speakers": "Jun Nakajima",
    "time": "2015-08-20T16:00:00-07:00",
    "title": "KVM as The NFV Hypervisor",
    "slides": []
  },
  {
    "speakers": "Pranith Kumar Karampuri",
    "time": "2015-08-20T16:00:00-07:00",
    "title": "Recent improvements in Gluster for VM image storage",
    "slides": []
  },
  {
    "speakers": "Julia Lawall",
    "time": "2015-08-20T16:00:00-07:00",
    "title": "Getting Started with Coccinelle (KVM edition)",
    "slides": []
  },
  {
    "speakers": "Liang Li",
    "time": "2015-08-20T16:30:00-07:00",
    "title": "KVM live migration optimization",
    "slides": []
  },
  {
    "speakers": "Mihai Caraman",
    "time": "2015-08-20T16:30:00-07:00",
    "title": "Low latency edge computing with QEMU/KVM: Challenges and future",
    "slides": []
  },
  {
    "speakers": "Julia Lawall",
    "time": "2015-08-20T16:30:00-07:00",
    "title": "Getting Started with Coccinelle (KVM edition)",
    "slides": []
  },
  {
    "speakers": "Mario Smarduch",
    "time": "2015-08-20T17:00:00-07:00",
    "title": "Migrating NFV applications to KVM Guest",
    "slides": []
  },
  {
    "speakers": "David Gibson",
    "time": "2015-08-20T17:00:00-07:00",
    "title": "Rethinking machine types",
    "slides": []
  },
  {
    "speakers": "Julia Lawall",
    "time": "2015-08-20T17:00:00-07:00",
    "title": "Getting Started with Coccinelle (KVM edition)",
    "slides": []
  },
  {
    "speakers": "Jiri Denemark",
    "time": "2015-08-21T09:00:00-07:00",
    "title": "Keynote Session: Libvirt status report",
    "slides": []
  },
  {
    "speakers": "Eric Blake",
    "time": "2015-08-21T09:15:00-07:00",
    "title": "Backing Chain management in QEMU and libvirt",
    "slides": []
  },
  {
    "speakers": "Michal Privoznik",
    "time": "2015-08-21T10:00:00-07:00",
    "title": "Libvirt: What did we do wrong?",
    "slides": []
  },
  {
    "speakers": "Martin Sivak",
    "time": "2015-08-21T11:15:00-07:00",
    "title": "oVirt and Gluster, hyper-converged!",
    "slides": []
  },
  {
    "speakers": "Nikola Dipanov",
    "time": "2015-08-21T12:00:00-07:00",
    "title": "High performance VMs in OpenStack",
    "slides": []
  },
  {
    "speakers": "Andrew Jones",
    "time": "2015-08-21T12:00:00-07:00",
    "title": "kvm-unit-tests: past, present, and future",
    "slides": []
  },
  {
    "speakers": "Martin Peřina",
    "time": "2015-08-21T12:00:00-07:00",
    "title": "The new oVirt Extension API - The 1st step for fully modular oVirt",
    "slides": []
  },
  {
    "speakers": "Piotr Kliczewski",
    "time": "2015-08-21T14:00:00-07:00",
    "title": "oVirt host communication - long way from an unpaved road to the highway",
    "slides": []
  },
  {
    "speakers": "Paolo Bonzini",
    "time": "2015-08-21T14:00:00-07:00",
    "title": "Securing secure boot: system management mode in KVM and Tiano Core",
    "slides": []
  },
  {
    "speakers": "Cleber Rosa",
    "time": "2015-08-21T14:00:00-07:00",
    "title": "Avocado: Next generation virt testing",
    "slides": []
  },
  {
    "speakers": "Jeff Cody",
    "time": "2015-08-21T14:30:00-07:00",
    "title": "Block Jobs: current status, upcoming challenges",
    "slides": []
  },
  {
    "speakers": "Simone Tiraboschi",
    "time": "2015-08-21T14:30:00-07:00",
    "title": "oVirt self-hosted engine seamless deployment",
    "slides": []
  },
  {
    "speakers": "Cleber Rosa",
    "time": "2015-08-21T14:30:00-07:00",
    "title": "Avocado: Next generation virt testing",
    "slides": []
  },
  {
    "speakers": "Fam Zheng",
    "time": "2015-08-21T15:30:00-07:00",
    "title": "Improving the QEMU Event Loop",
    "slides": []
  },
  {
    "speakers": "Weidong Han, Haobo Zhang",
    "time": "2015-08-21T15:30:00-07:00",
    "title": "Live migration with SR-IOV pass-through",
    "slides": []
  },
  {
    "speakers": "Arik Hadas",
    "time": "2015-08-21T15:30:00-07:00",
    "title": "Managed conversion of guests to oVirt",
    "slides": []
  },
  {
    "speakers": "Andre Przywara",
    "time": "2015-08-21T16:00:00-07:00",
    "title": "ARM interrupt virtualization",
    "slides": []
  },
  {
    "speakers": "Barak Azulay",
    "time": "2015-08-21T16:00:00-07:00",
    "title": "oVirt SR-IOV support",
    "slides": []
  },
  {
    "speakers": "Corey Minyard",
    "time": "2015-08-21T16:00:00-07:00",
    "title": "Using IPMI in QEMU",
    "slides": []
  }
]
sponsors:
  - type: "Gold Sponsors"
    who: ["IBM", "Red Hat (old logo)"]
  - type: "Silver Sponsor"
    who: ["HPE", "Proxmox"]
  - type: "Speaker Reception"
    who: ["OVA"]
---
The schedule and slides are available on [linux-kvm.org](https://linux-kvm.org/page/KVM_Forum_2015).

Videos are available on [YouTube](https://www.youtube.com/playlist?list=PLW3ep1uCIRfyLNSu708gWG7uvqlolk0ep).

# Pictures

* [Official pictures from the Linux Foundation](https://www.flickr.com/photos/linuxfoundation/albums/72157655545164444)

# Blogs

* [Two hypervisors, one great collaboration](https://www.redhat.com/en/blog/two-hypervisors-one-great-collaboration)
