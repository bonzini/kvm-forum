---
dates: "June 14-15, 2023"
place: "Brno, Czech Republic"
layout: archive
#submit: true
sponsors:
  - type: Organizer
    who: ["Red Hat"]
  - type: Gold Sponsor
    who: ["Arm"]
  - type: T-Shirt Sponsor
    who: ["Nutanix"]
pc: [paolo, daniel, alex, jan, christian, peter, karen, kevin]

# Obtained from https://kvm-forum.qemu.org/api/events/2023/submissions/
# using jq:
#
#    .results | map ({
#         "speakers": .speakers | map(.name) | join(", "),
#         "time" : .slot.start,
#         "title": .title,
#         "slides": (.resources[0].resource as $name |
#            if $name == null then
#              []
#            else
#              [$name | match("[^/]*$").string]
#            end) }) | sort_by(.time)
#
# and massaged slightly
# Likewise, to download resources I started from
#
#      curl https://kvm-forum.qemu.org/api/events/2023/submissions/ | \
#        jq -r '.results[].resources[0].resource | select(. != null)'
#
talks: [
  {
    "speakers": "Paolo Bonzini",
    "time": "2023-06-14T09:30:00+02:00",
    "title": "Keynote - KVM",
    "slides": [
       "kvmforum23-kvm_hFDKQ91.pdf"
    ]
  },
  {
    "speakers": "Alex Bennée",
    "time": "2023-06-14T09:45:00+02:00",
    "title": "Keynote - QEMU",
    "slides": [
       "https://www.bennee.com/~alex/presentations/kvm23-qemu-keynote.html"
    ]
  },
  {
    "speakers": "Fabiano Fidêncio",
    "time": "2023-06-14T10:00:00+02:00",
    "title": "Kata Containers - State of the Union",
    "slides": [
       "KVM_Forum_2023_-_Kata_Containers_-_State_of_the_Union_UysvfiU.pdf"
    ],
    "video": "https://www.youtube.com/watch?v=RoNElsa2h48&list=PLW3ep1uCIRfx5ApKGg41ARULGswMOCGBj&index=1&pp=iAQB"
  },
  {
    "speakers": "Nikunj A. Dadhania, Michael Roth",
    "time": "2023-06-14T10:30:00+02:00",
    "title": "Accounting and page migration challenges in Secure guests using FD-based private memory",
    "slides": [
       "gmem-and-memory-accounting_5zqBSyo.pdf"
    ],
    "video": "https://www.youtube.com/watch?v=c__UZ3pEQig&list=PLW3ep1uCIRfx5ApKGg41ARULGswMOCGBj&index=2&pp=iAQB"
  },
  {
    "speakers": "Christophe de Dinechin",
    "time": "2023-06-14T11:00:00+02:00",
    "title": "Chains of trust in Confidential Computing",
    "slides": [
       "KVM_Forum_2023_-_Smaller_0dFRlRm.pdf"
    ],
    "video": "https://www.youtube.com/watch?v=53kf4LY5YdM&list=PLW3ep1uCIRfx5ApKGg41ARULGswMOCGBj&index=3&pp=iAQB"
  },
  {
    "speakers": "Suzuki Kuruppassery Poulose",
    "time": "2023-06-14T11:30:00+02:00",
    "title": "KVM: Arm Confidential Compute Architecture Support",
    "slides": [
       "Arm_CCA_KVM_Support_GKucB4G.pdf"
    ],
    "video": "https://www.youtube.com/watch?v=RJfNM3pzZaE&list=PLW3ep1uCIRfx5ApKGg41ARULGswMOCGBj&index=4&pp=iAQB"
  },
  {
    "speakers": "Marc Zyngier, Oliver Upton",
    "time": "2023-06-14T12:00:00+02:00",
    "title": "KVM/arm64: Episode V - The Blob Strikes Back",
    "slides": [
       "slides_y4LoUW9.pdf"
    ],
    "video": "https://www.youtube.com/watch?v=Q6M4UgppnKs&list=PLW3ep1uCIRfx5ApKGg41ARULGswMOCGBj&index=5&pp=iAQB"
  },
  {
    "speakers": "Jeremy Powell",
    "time": "2023-06-14T14:00:00+02:00",
    "title": "AMD SEV-TIO: Trusted I/O for Secure Encrypted Virtualization",
    "slides": [],
    "video": "https://www.youtube.com/watch?v=WDs3cJNe7IE&list=PLW3ep1uCIRfx5ApKGg41ARULGswMOCGBj&index=6&pp=iAQB"
  },
  {
    "speakers": "Jörg Rödel",
    "time": "2023-06-14T14:30:00+02:00",
    "title": "The COCONUT Secure VM Service Module",
    "slides": [
       "COCONUT-SVSM_Pj9DVIQ.pdf"
    ],
    "video": "https://www.youtube.com/watch?v=zIcqetNmBmw&list=PLW3ep1uCIRfx5ApKGg41ARULGswMOCGBj&index=7&pp=iAQB"
  },
  {
    "speakers": "Claudio Carvalho",
    "time": "2023-06-14T15:00:00+02:00",
    "title": "Zero-Trust vTPM for AMD SEV-SNP Confidential Virtual Machines",
    "slides": [
       "zero-trust-vtpm-for-confidential-vms_MgGHpL8.pdf"
    ],
    "video": "https://www.youtube.com/watch?v=UEM0d1OdJ_k&list=PLW3ep1uCIRfx5ApKGg41ARULGswMOCGBj&index=8&pp=iAQB"
  },
  {
    "speakers": "Nicolas Saenz Julienne, Anel Orazgaliyeva",
    "time": "2023-06-15T09:15:00+02:00",
    "title": "Enabling Windows Credential Guard in KVM",
    "slides": [
       "kvm_forum_2023_-_final_-_15_jun_VvTXKsi.pdf"
    ],
    "video": "https://www.youtube.com/watch?v=QY7lg6aMaWc&list=PLW3ep1uCIRfx5ApKGg41ARULGswMOCGBj&index=9&pp=iAQB"
  },
  {
    "speakers": "Will Deacon",
    "time": "2023-06-15T09:45:00+02:00",
    "title": "Handling complex guest exits with eBPF",
    "slides": [
       "Handling_complex_guest_exits_with_eBPF_--_KVM_Forum_20_8gH6w3X.pdf"
    ],
    "video": "https://www.youtube.com/watch?v=nTMls33dG8Q&list=PLW3ep1uCIRfx5ApKGg41ARULGswMOCGBj&index=10&pp=iAQB"
  },
  {
    "speakers": "Alexander Graf",
    "time": "2023-06-15T10:15:00+02:00",
    "title": "macOS in QEMU (ARM edition)",
    "slides": [
       "macOS_in_QEMU_on_ARM_FhJY65D.pdf"
    ],
    "video": "https://www.youtube.com/watch?v=oZqFYJVOUQo&list=PLW3ep1uCIRfx5ApKGg41ARULGswMOCGBj&index=11&pp=iAQB"
  },
  {
    "speakers": "Hanna Czenczek, German Maglione",
    "time": "2023-06-15T11:00:00+02:00",
    "title": "virtio-fs – present and future",
    "slides": [
       "talk_O5WLFhA.pdf"
    ],
    "video": "https://www.youtube.com/watch?v=BhNz6quX6Dw&list=PLW3ep1uCIRfx5ApKGg41ARULGswMOCGBj&index=12&pp=iAQB"
  },
  {
    "speakers": "Kevin Wolf, Emanuele Giuseppe Esposito",
    "time": "2023-06-15T11:30:00+02:00",
    "title": "Multiqueue in the block layer",
    "slides": [
       "Multiqueue_in_the_block_layer_wLom4Bt.pdf"
    ],
    "video": "https://www.youtube.com/watch?v=Ubped0PgvZI&list=PLW3ep1uCIRfx5ApKGg41ARULGswMOCGBj&index=13&pp=iAQB"
  },
  {
    "speakers": "Eugenio Pérez, Gautam Dawar",
    "time": "2023-06-15T12:00:00+02:00",
    "title": "vDPA-net live migration with Shadow Virtqueue",
    "slides": [
       "vDPA_sw_lm_-_KVM2023_6Ix6R5i.pdf"
    ],
    "video": "https://www.youtube.com/watch?v=x9ARoNVzS04&list=PLW3ep1uCIRfx5ApKGg41ARULGswMOCGBj&index=14&pp=iAQB"
  },
  {
    "speakers": "Anton Johansson, Alessandro Di Federico",
    "time": "2023-06-15T14:00:00+02:00",
    "title": "Automatic promotion of helper functions to TCG using LLVM",
    "slides": [
       "auto_helper2tcg_promotion_YJ5Ar9v.pdf"
    ],
    "video": "https://www.youtube.com/watch?v=Gwz0kp7IZPE&list=PLW3ep1uCIRfx5ApKGg41ARULGswMOCGBj&index=15&pp=iAQB"
  },
  {
    "speakers": "Like Xu",
    "time": "2023-06-15T15:15:00+02:00",
    "title": "Live control of (most) CPU features via hybrid vCPU model",
    "slides": [
       "likexu_kvmforum2023_YoKow1p.pdf"
    ],
    "video": "https://www.youtube.com/watch?v=uNpN2mv-uBY&list=PLW3ep1uCIRfx5ApKGg41ARULGswMOCGBj&index=16&pp=iAQB"
  },
  {
    "speakers": "Salil Mehta, James Morse",
    "time": "2023-06-15T15:45:00+02:00",
    "title": "Challenges Revisited in Supporting Virt CPU Hotplug on architectures that don't Support CPU Hotplug (like ARM64)",
    "slides": [
       "KVM-forum-cpu-hotplug_7OJ1YyJ.pdf",
       "Challenges_Revisited_in_Supporting_Virt_CPU_Hotplug_-__ii0iNb3.pdf",
    ],
    "video": "https://www.youtube.com/watch?v=VdMkf06Pc6w&list=PLW3ep1uCIRfx5ApKGg41ARULGswMOCGBj&index=17&pp=iAQB"
  },
  {
    "speakers": "Cornelia Huck",
    "time": "2023-06-15T16:15:00+02:00",
    "title": "QEMU Arm CPU models and KVM",
    "slides": [
       "arm-cpu-models_3CFuvTn.pdf",
    ],
    "video": "https://www.youtube.com/watch?v=lEuYCq8teXs&list=PLW3ep1uCIRfx5ApKGg41ARULGswMOCGBj&index=18&pp=iAQB"
  }
]
---
## Presentations

{% include program.html %}

## Video

* [Video recordings](https://www.youtube.com/playlist?list=PLW3ep1uCIRfx5ApKGg41ARULGswMOCGBj)
* [Day 1 live stream](https://youtube.com/live/vy-3oYEDKCQ)
* [Day 2 live stream](https://youtube.com/live/hyrw4j2D6I0)
