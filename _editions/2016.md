---
dates: "August 24-26, 2016"
place: "Toronto, Canada"
layout: archive
sponsors:
  - type: "Platinum Sponsor"
    who: ["Huawei", "Red Hat (old logo)"]
  - type: "Gold Sponsors"
    who: ["IBM", "Intel"]
  - type: "Silver Sponsor"
    who: ["Arm"]
---
The schedule and slides are available on [linux-kvm.org](https://linux-kvm.org/page/KVM_Forum_2016).

Videos are available on [YouTube](https://www.youtube.com/playlist?list=PLW3ep1uCIRfzQoZ0SlniYE8nz1ZRobjH7).
