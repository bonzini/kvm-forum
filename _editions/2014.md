---
dates: "October 14-16, 2014"
place: "Düsseldorf, Germany"
layout: archive
talks: [
  {
    "speakers": "Paolo Bonzini",
    "time": "2014-10-14T09:00:00+02:00",
    "title": "Keynote: KVM",
    "slides": []
  },
  {
    "speakers": "Andrew Honig",
    "time": "2014-10-14T09:15:00+02:00",
    "title": "Security Hardening of KVM",
    "slides": []
  },
  {
    "speakers": "Alex Bennée",
    "time": "2014-10-14T10:00:00+02:00",
    "title": "Validating and Defending QEMU TCG Targets",
    "slides": []
  },
  {
    "speakers": "Eric Auger",
    "time": "2014-10-14T11:15:00+02:00",
    "title": "ARM KVM Platform Device Assignment",
    "slides": []
  },
  {
    "speakers": "Jun Nakajima",
    "time": "2014-10-14T12:00:00+02:00",
    "title": "Extending KVM Models Toward High-Performance NFV",
    "slides": []
  },
  {
    "speakers": "Bokdeuk Jeong",
    "time": "2014-10-14T14:00:00+02:00",
    "title": "Optimizing IO Virtualization and VM Memory Management for Tablet Devices",
    "slides": []
  },
  {
    "speakers": "Yashpal Dutta, Varun Sethi",
    "time": "2014-10-14T14:00:00+02:00",
    "title": "Hardware Accelerated Virtio Networking for NFV",
    "slides": []
  },
  {
    "speakers": "Rik van Riel",
    "time": "2014-10-14T14:30:00+02:00",
    "title": "Automatic NUMA Balancing",
    "slides": []
  },
  {
    "speakers": "Nicholas A. Bellinger",
    "time": "2014-10-14T14:30:00+02:00",
    "title": "vhost-scsi Technical Discussion",
    "slides": []
  },
  {
    "speakers": "Jike Song",
    "time": "2014-10-14T15:30:00+02:00",
    "title": "KvmGT: A Full GPU Virtualization Solution for KVM",
    "slides": []
  },
  {
    "speakers": "James Hogan",
    "time": "2014-10-14T15:30:00+02:00",
    "title": "KVM on MIPS",
    "slides": []
  },
  {
    "speakers": "Alex Williamson",
    "time": "2014-10-14T16:00:00+02:00",
    "title": "VFIO, OVMF, GPU, and You",
    "slides": []
  },
  {
    "speakers": "Paul Mackerras",
    "time": "2014-10-14T16:00:00+02:00",
    "title": "KVM on IBM POWER8 Machines",
    "slides": []
  },
  {
    "speakers": "Gerd Hoffmann",
    "time": "2014-10-14T16:30:00+02:00",
    "title": "Graphics in QEMU -- How the Guest Display Shows Up in Your Desktop Window",
    "slides": []
  },
  {
    "speakers": "Alexey Kardashevskiy",
    "time": "2014-10-14T16:30:00+02:00",
    "title": "VFIO on POWER",
    "slides": []
  },
  {
    "speakers": "Andreas Färber",
    "time": "2014-10-15T09:00:00+02:00",
    "title": "Keynote: QEMU",
    "slides": []
  },
  {
    "speakers": "Jeff Cody",
    "time": "2014-10-15T09:15:00+02:00",
    "title": "New to QEMU: A Developer's Guide to Contributing",
    "slides": []
  },
  {
    "speakers": "Bandan Das, Jan Kiszka, Yang Zhang",
    "time": "2014-10-15T10:00:00+02:00",
    "title": "Nested Virtualization - State of the Art and Future Directions",
    "slides": []
  },
  {
    "speakers": "Stefan Hajnoczi",
    "time": "2014-10-15T11:15:00+02:00",
    "title": "Towards Multi-Threaded Device Emulation in QEMU",
    "slides": []
  },
  {
    "speakers": "Andrea Arcangeli, Dr. David Alan Gilbert",
    "time": "2014-10-15T12:00:00+02:00",
    "title": "Memory Externalization With userfaultfd",
    "slides": []
  },
  {
    "speakers": "Pavel Dovgalyuk",
    "time": "2014-10-15T14:00:00+02:00",
    "title": "Deterministic Replay and Reverse Debugging in QEMU",
    "slides": []
  },
  {
    "speakers": "Ming Lei",
    "time": "2014-10-15T14:00:00+02:00",
    "title": "Virtio-blk Multi-queue Conversion and QEMU Optimization",
    "slides": []
  },
  {
    "speakers": "Juan Quintela",
    "time": "2014-10-15T14:30:00+02:00",
    "title": "Migration: Trying to Make it More Robust",
    "slides": []
  },
  {
    "speakers": "Max Reitz, Kevin Wolf",
    "time": "2014-10-15T14:30:00+02:00",
    "title": "More Block Device Configuration",
    "slides": []
  },
  {
    "speakers": "Jeff Cody",
    "time": "2014-10-15T15:30:00+02:00",
    "title": "Job Safety: Blockers in the Block Layer",
    "slides": []
  },
  {
    "speakers": "Raphael Sack",
    "time": "2014-10-15T15:30:00+02:00",
    "title": "KVM on Grid, Shaken Not Stirred",
    "slides": []
  },
  {
    "speakers": "Jiři Denemark",
    "time": "2014-10-16T09:00:00+02:00",
    "title": "Keynote: libvirt",
    "slides": []
  },
  {
    "speakers": "Daniel Berrange",
    "time": "2014-10-16T10:00:00+02:00",
    "title": "OpenStack Performance Optimization with NUMA, Huge Pages and CPU Pinning",
    "slides": []
  },
  {
    "speakers": "Adam Litke",
    "time": "2014-10-16T11:15:00+02:00",
    "title": "Thanks for Live Snapshots, Where's Live Merge?",
    "slides": []
  },
  {
    "speakers": "Michal Privoznik",
    "time": "2014-10-16T12:00:00+02:00",
    "title": "Libvirt. Why Should I Care?",
    "slides": []
  },
  {
    "speakers": "Hyotaek Shim",
    "time": "2014-10-16T14:00:00+02:00",
    "title": "I/O Demand-driven VM Scheduler in KVM",
    "slides": []
  },
  {
    "speakers": "Christian Bornträger",
    "time": "2014-10-16T14:30:00+02:00",
    "title": "KVM vs. Valgrind",
    "slides": []
  },
  {
    "speakers": "Paolo Bonzini",
    "time": "2014-10-16T15:30:00+02:00",
    "title": "QOM Exegesis and Apocalypse",
    "slides": []
  },
  {
    "speakers": "Marc Marí Barceló",
    "time": "2014-10-16T16:00:00+02:00",
    "title": "Testing QEMU Emulated Devices Using qtest",
    "slides": []
  }
]
sponsors:
  - type: "Platinum Sponsor"
    who: ["IBM"]
  - type: "Gold Sponsors"
    who: ["Huawei", "Red Hat (old logo)"]
  - type: "Silver Sponsors"
    who: ["Datera", "HPE", "OVA"]
---
The schedule and slides are available on [linux-kvm.org](https://linux-kvm.org/page/KVM_Forum_2014).

Videos are available on [YouTube](https://www.youtube.com/playlist?list=PLW3ep1uCIRfzXB9pWFIB9M0n2wcuLOsvJ).

# Blogs and news reports

* [The security state of KVM](https://lwn.net/Articles/619376/)
