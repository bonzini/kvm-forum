---
dates: "October 28-30, 2020"
layout: archive
talks: [
  {
    "speakers": "Dario Faggioli",
    "time": "2020-10-28T12:00:00+00:00",
    "title": "Virtual Topology for Virtual Machines: Friend or Foe?",
    "slides": [
      "Virtual%20Topology%20Friend%20or%20Foe.pdf"
    ]
  },
  {
    "speakers": "Janosch Frank",
    "time": "2020-10-28T13:00:00+00:00",
    "title": "The Common Challenges of Secure VMs",
    "slides": [
      "Janosch%20Frank.pdf"
    ]
  },
  {
    "speakers": "Will Deacon",
    "time": "2020-10-28T16:15:00+00:00",
    "title": "Virtualization for the Masses: Exposing KVM on Android",
    "slides": []
  },
  {
    "speakers": "Stefan Hajnoczi",
    "time": "2020-10-28T17:15:00+00:00",
    "title": "Optimizing for NVMe Drives: The 10 Microsecond Challenge",
    "slides": []
  },
  {
    "speakers": "Ofir Weisse, Alexandre Chartre",
    "time": "2020-10-28T18:30:00+00:00",
    "title": "KVM Address Space Isolation",
    "slides": [
      "KVM_2020_Address-Space-Isolation_Alex-Chartre_Ofir-Weisse.pdf"
    ]
  },
  {
    "speakers": "Richard W.M. Jones, Stefan Hajnoczi, Hubertus Franke, Susie Li, David Kaplan, Peter Maydell",
    "time": "2020-10-28T19:30:00+00:00",
    "title": "Panel Discussion: KVM-based Virtualization Contributor Q&A",
    "slides": []
  },
  {
    "speakers": "Christoffer Dall, Marc Zyngier",
    "time": "2020-10-29T06:00:00+00:00",
    "title": "Look Ma’, No (Real) Interrupt Controller!",
    "slides": []
  },
  {
    "speakers": "Jason Wang",
    "time": "2020-10-29T06:00:00+00:00",
    "title": "vDPA Support in Linux Kernel",
    "slides": [
      "vDPA_novideo.pdf"
    ]
  },
  {
    "speakers": "Wei Huang, Suravee Suthikulpanit",
    "time": "2020-10-29T06:30:00+00:00",
    "title": "AMD-vIOMMU: A Hardware-assisted Virtual IOMMU Technology",
    "slides": [
      "vIOMMU%20KVM%20Forum%202020.pdf"
    ]
  },
  {
    "speakers": "Jun Nakajima",
    "time": "2020-10-29T06:30:00+00:00",
    "title": "Evaluate Implementation Options of KVM-based Type1 (or 1.5) Hypervisor",
    "slides": [
      "Oct%2029_Evaluate%20implementation%20options%20of%20KVM-based_Jun%20Nakaijma.pdf"
    ]
  },
  {
    "speakers": "Ning Yang, Forrest Yuan Yu",
    "time": "2020-10-29T07:00:00+00:00",
    "title": "Hypervisor Based Integrity: Protect Guest Kernel in Cloud",
    "slides": [
      "KVM%20Forum%20-%20Hypervisor%20Based%20Integrity_%20Protect%20Guest%20Kernel%20in%20Cloud.pdf"
    ]
  },
  {
    "speakers": "Yu Zhang",
    "time": "2020-10-29T07:00:00+00:00",
    "title": "A Virtual IOMMU With Cooperative DMA Buffer Tracking",
    "slides": [
      "coIOMMU.pdf"
    ]
  },
  {
    "speakers": "Siqi Zhao",
    "time": "2020-10-29T07:30:00+00:00",
    "title": "Trap-less Virtual Interrupt for KVM on RISC-V",
    "slides": [
      "trap-less_virtual_interrupt_for_kvm_on_riscv.pdf"
    ]
  },
  {
    "speakers": "Liang Li",
    "time": "2020-10-29T07:30:00+00:00",
    "title": "Speed Up Creation of a VM With Pass Through GPU",
    "slides": [
      "Speed%20Up%20Creation%20of%20a%20VM%20With%20Passthrough%20GPU.pdf"
    ]
  },
  {
    "speakers": "Yulei Zhang",
    "time": "2020-10-29T10:00:00+00:00",
    "title": "Advanced Parallel Memory Virtualization",
    "slides": [
      "Advanced%20Parallel%20Memory%20Virtualization%20kvm%202020.pdf"
    ]
  },
  {
    "speakers": "Wanpeng Li",
    "time": "2020-10-29T10:00:00+00:00",
    "title": "KVM Latency Performance Tuning",
    "slides": [
      "KVM%20Latency%20and%20Scalability%20Performance%20Tuning.pdf"
    ]
  },
  {
    "speakers": "Yan Vugenfirer, Yansu Li",
    "time": "2020-10-29T10:30:00+00:00",
    "title": "Implementing SR-IOV Failover for Windows Guests During Migration",
    "slides": [
      "Implementing%20SR-IOV%20Failover%20for%20Windows%20Guests%20During%20Migration.pdf"
    ]
  },
  {
    "speakers": "Weinan Li",
    "time": "2020-10-29T10:30:00+00:00",
    "title": "The Practice Method to Speed Up 10x Boot-up Time for Guest in Alibaba Cloud",
    "slides": [
      "The%20Practice%20Method%20to%20Speed%20Up%2010x%20Boot-up%20Time%20for%20Guest%20in%20Alibaba%20Cloud.pdf"
    ]
  },
  {
    "speakers": "Christian Borntraeger",
    "time": "2020-10-29T14:00:00+00:00",
    "title": "Keynote: KVM",
    "slides": [
      "kvmforum2020_keynote_kvm.pdf"
    ]
  },
  {
    "speakers": "Paolo Bonzini",
    "time": "2020-10-29T14:15:00+00:00",
    "title": "Keynote: QEMU",
    "slides": [
      "kvmforum20-qemu.pdf"
    ]
  },
  {
    "speakers": "Bandan Das, Alexander Bulekov",
    "time": "2020-10-29T14:30:00+00:00",
    "title": "Virtual Device Fuzzing Support in QEMU",
    "slides": [
      "Fuzzing_KVMForum_2020.pdf"
    ]
  },
  {
    "speakers": "Andrew Jones",
    "time": "2020-10-29T14:30:00+00:00",
    "title": "KVM-unit-tests: When \"KVM\" Doesn't Mean KVM",
    "slides": [
      "kvm-unit-tests_%20When%20KVM%20doesn%27t%20mean%20_KVM_.pdf"
    ]
  },
  {
    "speakers": "Michael S. Tsirkin",
    "time": "2020-10-29T15:00:00+00:00",
    "title": "Virtual Versus Physical: Virtio Changes for New Hardware",
    "slides": [
      "virtual-versus-physical-2020-final.odp"
    ]
  },
  {
    "speakers": "Eric Auger",
    "time": "2020-10-29T15:00:00+00:00",
    "title": "A KVM-unit-tests and KVM selftests update for aarch64",
    "slides": [
      "kvm_unit_tests_selftests_update_for_aarch64_Eric_Auger.pdf"
    ]
  },
  {
    "speakers": "Daniel Berrangé",
    "time": "2020-10-29T15:30:00+00:00",
    "title": "Libvirt Status Report",
    "slides": []
  },
  {
    "speakers": "Liang Yan",
    "time": "2020-10-29T15:30:00+00:00",
    "title": "A Journey to Support vGPU in Firecracker",
    "slides": []
  },
  {
    "speakers": "Andreea Florescu",
    "time": "2020-10-29T15:45:00+00:00",
    "title": "Rust-vmm Status Report",
    "slides": [
      "KVM%20Forum%20rust-vmm%20-%20Status%20Report%202020.pdf"
    ]
  },
  {
    "speakers": "Salil Mehta",
    "time": "2020-10-29T16:00:00+00:00",
    "title": "Challenges in Supporting Virtual CPU Hotplug on SoC Based Systems (like ARM64)",
    "slides": [
      "Oct%2029_Challenges%20in%20Supporting%20Virtual%20CPU%20Hotplug%20in%20SoC%20Based%20Systems%20like%20ARM64_Salil%20Mehta.pdf"
    ]
  },
  {
    "speakers": "Ankur Arora",
    "time": "2020-10-29T16:00:00+00:00",
    "title": "Changing Paravirt Lock-ops for a Changing World",
    "slides": [
      "Dynamic-lock-ops--KVM-forum-2020.pdf"
    ]
  },
  {
    "speakers": "Yifei Jiang, Bo Wan",
    "time": "2020-10-29T16:30:00+00:00",
    "title": "HA-IOV: Applying Hardware-assisted Techniques to IO Virtualization Framework",
    "slides": [
      "HA-IOV.PDF"
    ]
  },
  {
    "speakers": "Sharan Santhanam",
    "time": "2020-10-29T16:30:00+00:00",
    "title": "Extremely Fast and Efficient NFV with Unikraft",
    "slides": [
      "dpdk.pdf"
    ]
  },
  {
    "speakers": "Wei Wang",
    "time": "2020-10-30T06:00:00+00:00",
    "title": "Live Migration With Hardware Acceleration",
    "slides": [
      "Oct%2030_Live%20Migration%20with%20Hardware%20Acceleration_Wei%20Wang%20.pdf"
    ]
  },
  {
    "speakers": "Sean Christopherson",
    "time": "2020-10-30T06:00:00+00:00",
    "title": "Intel Virtualization Technology Extensions to Enable Hardware Isolated VMs",
    "slides": [
      "TDX%20-%20KVM%20Forum%202020.pdf"
    ]
  },
  {
    "speakers": "Hao Wu",
    "time": "2020-10-30T06:30:00+00:00",
    "title": "Scalable Work Submission in Device Virtualization",
    "slides": [
      "Scalable_Work_Submission_In_Device_Virtualization.pdf"
    ]
  },
  {
    "speakers": "Isaku Yamahata",
    "time": "2020-10-30T06:30:00+00:00",
    "title": "Guest Memory Protection -- Current Status and Future",
    "slides": [
      "guest-memory-protection.pdf"
    ]
  },
  {
    "speakers": "Jacob Pan, Yi Liu",
    "time": "2020-10-30T07:00:00+00:00",
    "title": "PASID Management in KVM",
    "slides": [
      "KVM_forum_2020_PASID_MGMT_Yi_Jacob_final.pdf"
    ]
  },
  {
    "speakers": "Zhimin Feng",
    "time": "2020-10-30T07:00:00+00:00",
    "title": "KVM Live Upgrade with Properly Handling of Passthrough Devices",
    "slides": [
      "Kvm-live-upgrade-with-properly-handling%20of%20passthrough%20devices.pdf"
    ]
  },
  {
    "speakers": "Matias Vara Larsen",
    "time": "2020-10-30T07:30:00+00:00",
    "title": "Building a Cloud Infrastructure to Deploy Microservices as Microvm Guests",
    "slides": [
      "microtoro-kvmforum20.pdf"
    ]
  },
  {
    "speakers": "Jason Zeng",
    "time": "2020-10-30T07:30:00+00:00",
    "title": "Device Keepalive State for Local Live Migration and VMM Fast Restart",
    "slides": [
      "Device-Keepalive-State-KVMForum2020.pdf"
    ]
  },
  {
    "speakers": "Changpeng Liu, Xiaodong Liu",
    "time": "2020-10-30T10:00:00+00:00",
    "title": "Evolution of SPDK vhost Towards Secure Container Storage Service",
    "slides": [
      "KVM2020_Evolution%20Of%20SPDK%20towards%20secure%20container%20storage%20service_Xiaodong%20Liu%20and%20Changpeng%20Liu.pdf"
    ]
  },
  {
    "speakers": "Chao Gao",
    "time": "2020-10-30T10:00:00+00:00",
    "title": "Hypervisor-managed Linear Address Translation",
    "slides": [
      "kvm2020_hypervisor-managed%20linear%20address%20translation_v3.pdf"
    ]
  },
  {
    "speakers": "Raymond Zhang",
    "time": "2020-10-30T10:30:00+00:00",
    "title": "Debugging KVM Using Intel DCI Technology",
    "slides": []
  },
  {
    "speakers": "Qiao Hua, Zhou Yibo",
    "time": "2020-10-30T10:30:00+00:00",
    "title": "Minimizing VMExits in Private Cloud by Aggressive PV IPI and Passthrough Timer",
    "slides": [
      "Minimizing%20VMExits%20in%20Private%20Cloud%20by%20%20Aggressive%20PV%20IPI%20%20and%20Passthrough%20Timer.pdf"
    ]
  },
  {
    "speakers": "Stefano Garzarella",
    "time": "2020-10-30T14:00:00+00:00",
    "title": "Speeding Up VM’s I/O Sharing Host's io_uring Queues With Guests",
    "slides": [
      "KVMForum_2020_io_uring_passthrough_Stefano_Garzarella.pdf"
    ]
  },
  {
    "speakers": "Steven Sistare",
    "time": "2020-10-30T14:00:00+00:00",
    "title": "QEMU Live Update",
    "slides": [
      "qemu_live_update.pdf"
    ]
  },
  {
    "speakers": "David Hildenbrand, Michael S. Tsirkin",
    "time": "2020-10-30T14:30:00+00:00",
    "title": "Virtio-(balloon|pmem|mem): Managing Guest Memory",
    "slides": [
      "KVM%20Forum%202020%20Virtio-%28balloon%20pmem%20mem%29%20Managing%20Guest%20Memory.pdf"
    ]
  },
  {
    "speakers": "Alberto Garcia",
    "time": "2020-10-30T14:30:00+00:00",
    "title": "Faster and Smaller qcow2 Files With Subcluster-based Allocation",
    "slides": [
      "qcow2-subcluster-allocation.pdf"
    ]
  },
  {
    "speakers": "Mauricio Tavares",
    "time": "2020-10-30T15:00:00+00:00",
    "title": "Comparing Performance of NVMe Hard Drives in KVM, Baremetal, and Docker Using Fio and SPDK for Virtual Testbed Applications",
    "slides": [
      "KVM-Forum-2020_NVMe_BaremetalDockerKVM.pdf"
    ]
  },
  {
    "speakers": "Denis Lunev",
    "time": "2020-10-30T15:00:00+00:00",
    "title": "QEMU Snaphosts Are So Slow. Really?",
    "slides": [
      "FastVM%20snapshots.pdf"
    ]
  },
  {
    "speakers": "João Martins",
    "time": "2020-10-30T15:30:00+00:00",
    "title": "Towards an Alternative Memory Architecture",
    "slides": [
      "Towards%20an%20Alternative%20Memory%20Architecture.pdf"
    ]
  },
  {
    "speakers": "Eric Blake",
    "time": "2020-10-30T15:30:00+00:00",
    "title": "Bitmaps and NBD: Building Blocks of Change Block Tracking",
    "slides": [
      "kvmforum_2020_Bitmaps_and_NBD.pdf"
    ]
  },
  {
    "speakers": "Peter Xu",
    "time": "2020-10-30T16:00:00+00:00",
    "title": "KVM Dirty Ring - A New Approach to Logging",
    "slides": [
      "kvm_dirty_ring_peter.pdf"
    ]
  },
  {
    "speakers": "Yaowei Bai",
    "time": "2020-10-30T16:00:00+00:00",
    "title": "Bring SCSI Support Into QEMU Block Layer",
    "slides": [
      "Chinamobile-Bring%20SCSI%20support%20into%20QEMU%20block%20layer.pdf"
    ]
  },
  {
    "speakers": "Vitaly Kuznetsov, Vivek Goyal",
    "time": "2020-10-30T16:30:00+00:00",
    "title": "Long Live Asynchronous Page Fault!",
    "slides": [
      "KVMForum2020_APF.pdf"
    ]
  }
]
sponsors:
  - type: Platinum
    who: ["Red Hat"]
  - type: Silver
    who: ["Arm", "IBM"]
---
## Presentations

{% include program.html %}

## Videos

Video recordings are available on [YouTube](https://www.youtube.com/watch?v=8yA2SNnx2Ko&list=PLW3ep1uCIRfxcUjrH2zcnTmav3mktDwe8).
