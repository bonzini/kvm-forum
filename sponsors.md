---
title: Sponsors
layout: page
permalink: /sponsors/
---
{% include relative_root.html %}

We would like to thank our sponsors for contributing to the organization
of KVM Forum.

{% include sponsors.html %}
